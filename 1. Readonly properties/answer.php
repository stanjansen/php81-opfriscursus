<?php

declare(strict_types=1);

/**
 * Readonly: https://php.watch/versions/8.1/readonly
 *
 * Use readonly properties!
 */

class ImmutableMonkeyThatsAllergicToBananas
{
    public function __construct(
        public readonly int $bananaCount, // Trailing comma's to reduce merge conflicts when new properties get added.
    ) {
    }
}




// Do not edit below.
$monkey = new ImmutableMonkeyThatsAllergicToBananas(1337);

echo 'The monkey should still have 1337 bananas.'.PHP_EOL;
var_dump($monkey);
