<?php

declare(strict_types=1);

/**
 * Readonly: https://php.watch/versions/8.1/readonly
 *
 * Use readonly properties!
 */

class ImmutableMonkeyThatsAllergicToBananas
{
    public function __construct(
        private int $bananaCount
    ) {
    }

    public function getBananaCount(): int
    {
        return $this->bananaCount;
    }
}




// Do not edit below.
$monkey = new ImmutableMonkeyThatsAllergicToBananas(1337);

echo 'The monkey should still have 1337 bananas.'.PHP_EOL;
var_dump($monkey);
