<?php

declare(strict_types=1);

/**
 * You don't have to run this file. It's just a list of examples of smaller features.
 */


/**
 * 1. Null-safe operator: https://php.watch/versions/8.0/null-safe-operator
 */
$iban = $user->getAccount()?->getIban();


/**
 * 2. Non-capturing catches: https://wiki.php.net/rfc/non-capturing_catches
 */
try {
    throw new Exception('foo');
} catch (Exception) {
    // Do nothing...
}


/**
 * 3. Stringable interface: https://php.watch/versions/8.0/stringable
 */
class Text implements Stringable // If it would not have been implemented, PHP would magically do this instead, let's enforce it in our code for consistency and non-magic.
{
    public function __toString(): string
    {
        return 'The goat has been milked.';
    }
}


/**
 * 4. new str_ functions: https://www.amitmerchant.com/new-string-functions-php8/
 */
$fullText = 'Choong has cool new NFT shoes';
$subText = 'NFT';

$contains = str_contains($fullText, $subText);
$startsWith = str_starts_with($fullText, $subText);
$endsWith = str_ends_with($fullText, $subText);


/**
 * 5. First-class Callable syntax: https://php.watch/versions/8.1/first-class-callable-syntax
 *
 * Don't edit the class please :)
 */

class TestClass
{
    public function callCallable(callable $callable, ...$params): mixed // Remember mixed? ;)
    {
        return $callable($params);
    }

    public function foo(): string
    {
        return 'bar';
    }
}

$testClass = new TestClass();
$testClass->callCallable($testClass->foo(...));
$testClass->callCallable(var_dump(...), 'bar');


/**
 * 6. Intersection types: https://php.watch/versions/8.1/intersection-types
 */
class Foo
{
    public function doStuff(FromArrayable&JsonSerializable $model): void
    {
        ...
    }
}


/**
 * 7. Replacement for get_class: https://php.watch/versions/8.0/class-constant-on-objects
 */
$testClass::class;


/**
 * 8. Array unpacking support for string-keyed arrays: https://php.watch/versions/8.1/spread-operator-string-array-keys
 */
$array1 = ['foo' => 'bar'];
$array2 = ['foo' => 'bar', 'bar' => 'baz'];
[
    ...$array1,
    ...$array2,
    ...['extra' => 'i am special'],
];


/**
 * 9. Final class constants: https://php.watch/versions/8.1/final-class-const
 */
class Bar
{
    final public const BAZ = 'baz'; // This is applied everywhere in our codebase.
}


/**
 * 10. Throw is now an expression: https://php.watch/versions/8.0/throw-expressions
 */
$user = $this->getUser() ?: throw new Exception('User pl0x');