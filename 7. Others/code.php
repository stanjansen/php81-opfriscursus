<?php

declare(strict_types=1);

/**
 * You don't have to run this file. It's just a list of examples of smaller features.
 */


/**
 * 1. Null-safe operator: https://php.watch/versions/8.0/null-safe-operator
 */
$account = $user->getAccount();
$iban = $account->getIban() : null;


/**
 * 2. Non-capturing catches: https://wiki.php.net/rfc/non-capturing_catches
 */
try {
    throw new Exception('foo');
} catch (Exception $exception) {
    // Do nothing...
}


/**
 * 3. Stringable interface: https://php.watch/versions/8.0/stringable
 */
class Text
{
    public function __toString(): string
    {
        return 'The goat has been milked.';
    }
}


/**
 * 4. new str_ functions: https://www.amitmerchant.com/new-string-functions-php8/
 */
$fullText = 'Choong has cool new NFT shoes';
$subText = 'NFT';

$contains = false !== strpos($fullText, $subText);
$startsWith = 0 === strpos($fullText, $subText);
$endsWith = $subText === substr($fullText, -strlen($subText));


/**
 * 5. First-class Callable syntax: https://php.watch/versions/8.1/first-class-callable-syntax
 *
 * Don't edit the class please :)
 */

class TestClass
{
    /**
     * @return mixed
     */
    public function callCallable(callable $callable, ...$params)
    {
        return $callable($params);
    }

    public function foo(): string
    {
        return 'bar';
    }
}

$testClass = new TestClass();
$testClass->callCallable([$testClass, 'foo']);
$testClass->callCallable(Closure::fromCallable('var_dump'), 'bar');


/**
 * 6. Intersection types: https://php.watch/versions/8.1/intersection-types
 */
class Foo
{
    public function doStuff(FromArrayable $model): void
    {
        if (!$model instanceof JsonSerializable) {
            throw new InvalidArgumentException('$model must be JsonSerializable.');
        }
    }
}


/**
 * 7. Replacement for get_class: https://php.watch/versions/8.0/class-constant-on-objects
 */
get_class($testClass);


/**
 * 8. Array unpacking support for string-keyed arrays: https://php.watch/versions/8.1/spread-operator-string-array-keys
 */
$array1 = ['foo' => 'bar'];
$array2 = ['foo' => 'bar', 'bar' => 'baz'];
array_merge($array1, $array2, ['extra' => 'i am special']);


/**
 * 9. Final class constants: https://php.watch/versions/8.1/final-class-const
 */
class Bar
{
    public const BAZ = 'baz';
}


/**
 * 10. Throw is now an expression: https://php.watch/versions/8.0/throw-expressions
 */
$user = $this->getUser();
if (null === $user) {
    throw new Exception('User pl0x');
}