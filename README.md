# PHP81 opfriscursus

## Installation
Welcome to the almighty "PHP8.1 opfriscursus"!

To get started, follow these simple steps:
- Spin up the (new) development environment
- Enter your PHP8.1 container: `docker exec -ti dev_php81_1 /bin/bash`
- Run: `cd /app && git clone git@gitlab.com:stanjansen/php81-opfriscursus.git`
- Open the `php81-opfriscursus` app via Gateway
- Set your PHP version to `PHP: 8.1` (in the toolbar at the bottom right)

## Get hands-on!
There is a subfolder for each feature containing a `code.php` and `answer.php`. DO NOT CHEAT BY LOOKING AT THE `answer.php` FIRST (or by looking at the PHPStorm suggestions)!

The `code.php` contains some PHP7.4 code that can be upgraded to a new cool PHP8 feature (and sometimes a mix of a previous feature). 
Try your best upgrading it (a link to the explanation of the feature is in the comments).

When finished, run it using `php [YOURFOLDER]/code.php`. If it runs and you think you're done, compare it to the answer :)
There are also some comments in the answer for explanations and some extra's.

## Goodbye
Now you're an expert, you are free to delete this folder. Bye :(