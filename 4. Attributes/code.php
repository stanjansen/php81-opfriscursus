<?php

declare(strict_types=1);

/**
 * Attributes: https://php.watch/versions/8.0/attributes
 *
 * Edit all you want!
 */

/**
 * @Annotation
 */
class UserEmailNotVerified //extends Constraint (in ouw own codebase ;) )
{
}

class User
{
    /**
     * @UserEmailNotVerified
     */
    public string $email;
}






// Do not change below!

$reflector = new ReflectionClass(User::class);
$attributes = $reflector->getProperty('email')->getAttributes();

echo 1 === count($attributes) && UserEmailNotVerified::class === $attributes[0]->getName() ? 'Good job :)' : 'Convert the annotation to an attribute.';
echo PHP_EOL;