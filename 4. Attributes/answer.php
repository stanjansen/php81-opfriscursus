<?php

declare(strict_types=1);

/**
 * Attributes: https://php.watch/versions/8.0/attributes
 *
 * Edit all you want!
 */

#[Attribute]
class UserEmailNotVerified //extends Constraint (in ouw own codebase ;) )
{
}

class User
{
    /**
     * Annotations are still possible aswell.
     * Attributes accept plain PHP instead of JSON like annotations do. So you can use statics and named arguments,
     * example: #[Entity(repositoryClass: UserRepository::class, readOnly: false)]
     */
    #[UserEmailNotVerified]
    public string $email;
}





// Do not change below!

$reflector = new ReflectionClass(User::class);
$attributes = $reflector->getProperty('email')->getAttributes();

echo 1 === count($attributes) && UserEmailNotVerified::class === $attributes[0]->getName() ? 'Good job :)' : 'Convert the annotation to an attribute.';
echo PHP_EOL;