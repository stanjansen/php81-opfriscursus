<?php

declare(strict_types=1);

/**
 * Match: https://php.watch/versions/8.0/match-expression
 *
 * Use a match!
 */

class SuperSmartLocaleToLanguageCalculator
{
    public static function getLanguageForLocale(string $locale): string
    {
        switch ($locale) {
            case 'nl_NL':
                return 'NL';

            case 'de_DE':
                return 'DE';
        }

        throw new Exception('Locale not supported.');
    }
}




// Do not edit below.
$language = SuperSmartLocaleToLanguageCalculator::getLanguageForLocale('nl_NL');
if ('NL' !== $language) {
    throw new Exception('Something went wrong...');
}

try {
    SuperSmartLocaleToLanguageCalculator::getLanguageForLocale('foo');
} catch (Throwable) {
    echo 'Woooo exception!'.PHP_EOL;
    die();
}

throw new Exception('Uh oh... something went wrong...');