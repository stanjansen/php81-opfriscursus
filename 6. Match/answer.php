<?php

declare(strict_types=1);

/**
 * Match: https://php.watch/versions/8.0/match-expression
 *
 * Use a match!
 */

class SuperSmartLocaleToLanguageCalculator
{
    public static function getLanguageForLocale(string $locale): string
    {
        return match ($locale) {
            'nl_NL' => 'NL',
            'de_DE' => 'DE',
        };
    }
}




// Do not edit below.
$language = SuperSmartLocaleToLanguageCalculator::getLanguageForLocale('nl_NL');
if ('NL' !== $language) {
    throw new Exception('Something went wrong...');
}

try {
    SuperSmartLocaleToLanguageCalculator::getLanguageForLocale('foo');
} catch (Throwable) {
    echo 'Woooo exception!'.PHP_EOL;
    die();
}

throw new Exception('Uh oh... something went wrong...');