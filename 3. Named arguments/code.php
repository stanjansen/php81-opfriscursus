<?php

declare(strict_types=1);

/**
 * Named arguments: https://php.watch/versions/8.0/named-parameters
 *
 * Edit all you want!
 */
class Bicycle
{
    public string $brand;
    public int $wheels;
    public bool $isOffroad;
    public bool $isElectric;

    public function __construct(
        string $brand,
        int $wheels = 2,
        bool $isOffroad = false,
        bool $isElectric = false,
    ) {
        $this->brand = $brand;
        $this->wheels = $wheels;
        $this->isOffroad = $isOffroad;
        $this->isElectric = $isElectric;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['brand'],
            $data['wheels'] ?? 2,
            $data['isOffroad'] ?? false,
            $data['isElectric'] ?? false,
        );
    }
}

// New news to stay new and fromArray needs to keep using an array.
$uitgaanBicycle = new Bicycle('CheapassBikes');
$unicycle = new Bicycle('Clowntjes BV', 1);
$mountainbike = new Bicycle('DrekTrekker', 2, true);
$electricBicycle = new Bicycle('Tesla', 2, false, true);
$electricTricycle = Bicycle::fromArray([
    'brand' => 'Oude mensen BV',
    'wheels' => 3,
    'isElectric' => true,
]);





// Don't edit below!
function assertSame(mixed $actual, mixed $expected): void
{
    if ($actual !== $expected) {
        throw new LogicException($actual.' does not match '.$expected);
    }
}

echo 'Testing uitgaanBicycle'.PHP_EOL;
assertSame('CheapassBikes', $uitgaanBicycle->brand);

echo 'Testing unicycle'.PHP_EOL;
assertSame(1, $unicycle->wheels);

echo 'Testing mountainbike'.PHP_EOL;
assertSame(2, $mountainbike->wheels);
assertSame(true, $mountainbike->isOffroad);

echo 'Testing electricBicycle'.PHP_EOL;
assertSame(2, $electricBicycle->wheels);
assertSame(false, $electricBicycle->isOffroad);
assertSame(true, $electricBicycle->isElectric);

echo 'Testing electricTricycle'.PHP_EOL;
assertSame(3, $electricTricycle->wheels);
assertSame(false, $electricTricycle->isOffroad);
assertSame(true, $electricTricycle->isElectric);

echo 'Great success!'.PHP_EOL;
