<?php

declare(strict_types=1);

/**
 * Named arguments: https://php.watch/versions/8.0/named-parameters
 *
 * Edit all you want!
 */
class Bicycle
{
    public function __construct(
        public readonly string $brand, // Readonly is especially useful with public properties if you don't expect them to change :) No longer need for making it private and adding a getter only.
        public readonly int $wheels = 2,
        public readonly bool $isOffroad = false,
        public readonly bool $isElectric = false,
    ) {}
}

// New news to stay new and fromArray needs to keep using an array.
$uitgaanBicycle = new Bicycle('CheapassBikes');
$unicycle = new Bicycle('Clowntjes BV', wheels: 1);
$mountainbike = new Bicycle('DrekTrekker', isOffroad: true);
$electricBicycle = new Bicycle('Tesla', isElectric: true);
$electricTricycle = new Bicycle(...[
    'brand' => 'Oude mensen BV',
    'wheels' => 3,
    'isElectric' => true,
]);





// Don't edit below!
function assertSame(mixed $actual, mixed $expected): void
{
    if ($actual !== $expected) {
        throw new LogicException($actual.' does not match '.$expected);
    }
}

echo 'Testing uitgaanBicycle'.PHP_EOL;
assertSame('CheapassBikes', $uitgaanBicycle->brand);

echo 'Testing unicycle'.PHP_EOL;
assertSame(1, $unicycle->wheels);

echo 'Testing mountainbike'.PHP_EOL;
assertSame(2, $mountainbike->wheels);
assertSame(true, $mountainbike->isOffroad);

echo 'Testing electricBicycle'.PHP_EOL;
assertSame(2, $electricBicycle->wheels);
assertSame(false, $electricBicycle->isOffroad);
assertSame(true, $electricBicycle->isElectric);

echo 'Testing electricTricycle'.PHP_EOL;
assertSame(3, $electricTricycle->wheels);
assertSame(false, $electricTricycle->isOffroad);
assertSame(true, $electricTricycle->isElectric);

echo 'Great success!'.PHP_EOL;
