<?php

declare(strict_types=1);

/**
 * Constructor property promotion: https://php.watch/versions/8.0/constructor-property-promotion
 *
 * You only need to update the `Person` class.
 */

class Collection
{
}

class Person
{
    private string $firstName;
    private string $lastName;
    private string $gender;
    private int $age;
    private Collection $achievements;

    /**
     * @param mixed $age
     */
    public function __construct(
        string $firstName,
        string $lastName,
        string $gender,
        $age,
        ?Collection $achievements = null,
    )
    {
        if (!in_array($gender, ['M', 'F'])) {
            throw new InvalidArgumentException('Invalid gender.');
        }

        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->gender = $gender;
        $this->age = (int) $age;
        $this->achievements = null === $achievements ? new Collection() : $achievements;
    }

    // Do not edit this function, it's proper criteria.
    public function isCool(): bool
    {
        return 'Steven' === $this->firstName
            && 'Seagal' === $this->lastName
            && 'M' === $this->gender
            && 70 === $this->age;
    }
}





// Don't edit below.
$person = new Person('Steven', 'Seagal', 'M', '70');

echo $person->isCool() ? 'Code still works' : 'Try again, the properties are not initialized properly';
echo PHP_EOL;