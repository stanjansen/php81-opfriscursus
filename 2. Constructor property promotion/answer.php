<?php

declare(strict_types=1);

/**
 * Constructor property promotion: https://php.watch/versions/8.0/constructor-property-promotion
 *
 * You only need to update the `Person` class.
 */

class Collection
{
}

class Person
{
    private readonly int $age; // Still needed as it has logic in the constructor to cast it.

    public function __construct(
        private readonly string $firstName, // Readonly can be used here :) Sadly rector could not apply this to our codebase because the rule would have too much conflicts.
        private readonly string $lastName,
        private readonly string $gender,
        mixed $age, // Mixed is now a type.
        private readonly Collection $collection = new Collection(), // You can now do `new` in params :) (be careful though)
    ) {
        if (!in_array($gender, ['M', 'F'])) {
            throw new InvalidArgumentException('Invalid gender.');
        }

        $this->age = (int) $age;
    }

    // Do not edit this function, it's proper criteria.
    public function isCool(): bool
    {
        return 'Steven' === $this->firstName
            && 'Seagal' === $this->lastName
            && 'M' === $this->gender
            && 70 === $this->age;
    }
}

// Don't edit below.
$person = new Person('Steven', 'Seagal', 'M', '70');

echo $person->isCool() ? 'Code still works' : 'Try again, the properties are not initialized properly';
echo PHP_EOL;