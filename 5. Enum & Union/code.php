<?php

declare(strict_types=1);

/**
 * Enums: https://php.watch/versions/8.1/enums
 * Union types: https://php.watch/versions/8.0/union-types
 *
 * Transform the OrderStatus to an enum.
 */

class OrderStatus
{
    public const NEW = 'new';
    public const COMPLETED = 'completed';

    public static function getAll(): array
    {
        return [
            self::NEW,
            self::COMPLETED,
        ];
    }
}

class Order
{
    private string $status;

    public function setStatus(string $status): void
    {
        if (!in_array($status, OrderStatus::getAll())) {
            throw new InvalidArgumentException('Invalid order status');
        }

        $this->status = $status;
    }
}

$statusCount = count(OrderStatus::getAll());







// Don't edit below.
$order = new Order();
$order->setStatus('new');

if (!enum_exists(OrderStatus::class)) {
    throw new Exception('Transform the OrderStatus class to an enum!');
}

echo 2 === $statusCount ? 'Code works!' : 'Uh oh, I expected 2 order statuses';
echo PHP_EOL;