<?php

declare(strict_types=1);

/**
 * Enums: https://php.watch/versions/8.1/enums
 * Union types: https://php.watch/versions/8.0/union-types
 *
 * Transform the OrderStatus to an enum.
 */

enum OrderStatus: string
{
    case New = 'new';
    case Completed = 'completed';
}

class Order
{
    private OrderStatus $status;

    public function setStatus(OrderStatus|string $status): void // Union types can be used optionally for easier usage, just an example though ;)
    {
        // No need to check anymore as ::from gives an error if it does not exist. You could also use ::tryFrom if you don't want an exception instead!

        $this->status = is_string($status) ? OrderStatus::from($status) : $status;
    }
}

$statusCount = count(OrderStatus::cases());






// Don't edit below.
$order = new Order();
$order->setStatus('new');

if (!enum_exists(OrderStatus::class)) {
    throw new Exception('Transform the OrderStatus class to an enum!');
}

echo 2 === $statusCount ? 'Code works!' : 'Uh oh, I expected 2 order statuses';
echo PHP_EOL;